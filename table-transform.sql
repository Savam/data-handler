create table FIRST(
  id varchar(256),
  name varchar(256)
);

insert into FIRST values ('1', 'name1');
insert into FIRST values ('2', 'name2');
insert into FIRST values ('3', 'name3');

CREATE TABLE second (name1 varchar(256), name2 varchar(256), name3 varchar(256)) AS
  select f1.id, f2.id, f3.id from first as f1, first as f2, first as f3
  where (f1.name = 'name1') AND (f2.name = 'name2') AND (f3.name = 'name3');

select * from first;
select * from second;