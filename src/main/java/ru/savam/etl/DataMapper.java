package ru.savam.etl;

import org.apache.hadoop.hive.ql.io.orc.OrcSerde;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfo;
import org.apache.hadoop.hive.serde2.typeinfo.TypeInfoUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class DataMapper extends Mapper<LongWritable, Text, NullWritable, Writable> {

    private final static Logger logger = Logger.getLogger(DataMapper.class);

    private final OrcSerde serde = new OrcSerde();

    private final String typeString = "struct<id:int,first_name:string,last_name:string,account_number:int,email:string>";

    private final TypeInfo typeInfo = TypeInfoUtils
            .getTypeInfoFromTypeString(typeString);
    private final ObjectInspector oip = TypeInfoUtils
            .getStandardJavaObjectInspectorFromTypeInfo(typeInfo);

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String[] split = value.toString().split("\t+");

        if (split[1].matches("-") || split[2].matches("-")) {
            logger.error("Name is missing in this row: " + value);
        }

        if (Pattern.compile(".*#$").matcher(split[2]).matches()) {
            logger.error("Double or ambiguous name: " + value);
        }

        if (split[3].contains("-") || split[3].contains("/")) {
            logger.error("Incorrect account number: " + value);
            split[3] = split[3].replaceAll("-", "");
            split[3] = split[3].replaceAll("/", "");
        }

        List<Object> struct = new ArrayList<>();
        struct.add(0, Integer.valueOf(split[0]));
        struct.add(1, split[1]);
        struct.add(2, split[2]);
        struct.add(3, Integer.valueOf(split[3]));
        struct.add(4, split[4]);

        Writable row = serde.serialize(struct, oip);

        context.write(NullWritable.get(), row);
    }
}
