package ru.savam.etl;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.io.orc.OrcNewOutputFormat;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class Handler extends Configured implements Tool {

    public static void main(String[] args) throws Exception {

        int res = ToolRunner.run(new Configuration(), new Handler(), args);
        System.exit(res);
    }

    public int run(String[] arg) throws Exception {
        Configuration conf = getConf();

        conf.set("orc.create.index", "true");

        Job job = Job.getInstance(conf);
        job.setJarByClass(Handler.class);
        job.setJobName("ORC Output");
        job.setMapOutputKeyClass(NullWritable.class);
        job.setMapOutputValueClass(Writable.class);


        job.setMapperClass(DataMapper.class);

        job.setNumReduceTasks(Integer.parseInt(arg[2]));
        job.setOutputFormatClass(OrcNewOutputFormat.class);

        FileInputFormat.addInputPath(job, new Path(arg[0]));
        Path output = new Path(arg[1]);

        OrcNewOutputFormat.setCompressOutput(job, true);
        OrcNewOutputFormat.setOutputPath(job, output);

        return job.waitForCompletion(true) ? 0 : 1;
    }


}