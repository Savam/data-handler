package ru.savam.transform;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
// отельный класс для выравнивания строк по паттерну
public class PatternMatcher {

    private String[] tokens;

    private String id;
    private String firstName;
    private String secondName;
    private String accountNumber;
    private String email;

    private Matcher digitMatcher;
    private Matcher nameMatcher1;
    private Matcher nameMatcher2;
    private Matcher threeDigitMatcher;
    private Matcher sixDigitMatcher;
    private Matcher emailMatcher;


    private PatternMatcher() {

    }

    public PatternMatcher(String[] tokens) {
        this.tokens = tokens;

        this.id = tokens[0];
        this.firstName = tokens[1];
        this.secondName = tokens[2];
        this.accountNumber = tokens[3];
        this.email = tokens[4];

        digitMatcher = Pattern.compile("\\d+").matcher(id);
        nameMatcher1 = Pattern.compile("[a-zA-Z]+").matcher(firstName);
        nameMatcher2 = Pattern.compile("[a-zA-Z]+").matcher(secondName);
        threeDigitMatcher = Pattern.compile("\\d{3}.*").matcher(accountNumber);
        sixDigitMatcher = Pattern.compile("\\d{6}$").matcher(accountNumber);
        emailMatcher = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE).matcher(email);
    }

    public Boolean isLineValidByPattern() {
        trimArray();
        if (!digitMatcher.matches()) {
            return false;
        } else if (!nameMatcher1.matches()) {
            return false;
        } else if (!nameMatcher2.matches()) {
            return false;
        } else if (!sixDigitMatcher.matches()) {
            if (threeDigitMatcher.matches()) {
                tokens[3] = tokens[3].replaceAll("-", "");
            }
            return false;
        } else if (!emailMatcher.matches()) {
            return false;
        }
        return true;
    }

//    public String[] fixByPattern() {
//        trimArray();
//        if (threeDigitMatcher.matches()) {
//            tokens[3] = tokens[3].replaceAll("-", "");
//            tokens[3] = tokens[3].replaceAll("/", "");
//        }
//        return tokens;
//    }

    private String[] trimArray() {
        return Arrays.stream(tokens).map(String::trim).toArray(String[]::new);
    }


}
