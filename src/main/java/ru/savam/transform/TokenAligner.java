package ru.savam.transform;

import org.apache.commons.lang3.ArrayUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Pattern;

public class TokenAligner {

    public void run() {
        File file = new File("data/data.tsv");

        Charset inputEncoding = Charset.forName("UTF-16LE");
        Charset outputEncoding = Charset.forName("UTF-8");

        try (InputStream in = new FileInputStream(file);
             InputStreamReader inputStreamReader = new InputStreamReader(in, inputEncoding);
             BufferedReader br = new BufferedReader(inputStreamReader);

             OutputStream out = new FileOutputStream("input/data.tsv");
             OutputStreamWriter outputStreamWriter = new OutputStreamWriter(out, outputEncoding)
        ) {
            Scanner sc = new Scanner(br);
            sc.useDelimiter(Pattern.compile("\n"));
            sc.next();

            while (sc.hasNext()) {

                String[] currentLine = sc.next().split("\t");

                currentLine = removeBlankSpaceAndEmptyString(currentLine);

                String[] strings = Arrays.stream(currentLine).map(TokenAligner::handleToken).toArray(String[]::new);

                String[] constructedString = alignStringWithIncorrectLength(strings, sc); // построчная обработка строк

                constructedString = Arrays.stream(constructedString).map(word -> word + "\t").toArray(String[]::new);

                StringBuilder stringBuilder = new StringBuilder();
                for (String aConstructedString : constructedString) {
                    stringBuilder.append(aConstructedString);
                }

                outputStreamWriter.write(stringBuilder.toString());
                outputStreamWriter.write("\n");
            }
        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }
    }

    // если недостаточно слов в строке, то добавить к ней следующую строку с помощью рекрсии и обработать
    private static String[] alignStringWithIncorrectLength(String[] strings, Scanner sc) {
        if (strings.length < 5) {
            String[] nextLine = sc.next().split("\t");

            nextLine = removeBlankSpaceAndEmptyString(nextLine);

            String[] concatLine = ArrayUtils.addAll(strings, nextLine);

            concatLine = appendTagIfLineIsIncorrect(concatLine);

            String[] constructString = alignStringWithIncorrectLength(concatLine, sc);

            return Arrays.stream(constructString).map(TokenAligner::handleToken).toArray(String[]::new);
        }
        return strings;
    }

    // добавление тэга для обработки ошибок
    private static String[] appendTagIfLineIsIncorrect(String[] concatLine) {
        if (concatLine.length >= 5 && !new PatternMatcher(concatLine).isLineValidByPattern() && !Pattern.compile(".*#$").matcher(concatLine[2]).matches()) {
            concatLine = ArrayUtils.remove(concatLine, 3);
            concatLine[2] = concatLine[2] + " #";
        }
        return concatLine;
    }

    // удаление пустых строк
    private static String[] removeBlankSpaceAndEmptyString(String[] nextLine) {
        if (nextLine[0].matches("^$") || nextLine[0].matches("^\\s*$")) {
            nextLine = ArrayUtils.remove(nextLine, 0);
        }
        return nextLine;
    }

    // если слово пропущено, то добавить прочерк
    private static String handleToken(String word) {
        word = word.trim();
        return word.replaceAll("^$", "-");
    }
}
