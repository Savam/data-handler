Задача 1.
    Программа трансформации битого файла в папке "transform".
    
    Выравнивание: 
        * Если слово пропущено, проставляется "-"
        * Если некорректное количество имен, проставляется тэг "#"
    
Задача 2.    
    Программа отображения данных и логирование ошибок в папке "etl".
    В программе задается схема для преобразрвание в ORC-файл, для последующей обработки инструментом Hive.
    С помощью тегов заданных в прогамме трансформации, все ошибки в данных логируются в файл.
    Для реализации задачи обработки данных использоваолся псевдо-кластер Hadoop.


    Для создания таблицы в Hive из полученных данных использовалась команда:
        
    create external table acc_info (id int, first_name string, last_name string, account_number int, email string) stored as orc location "/output";

Задача 3.
    
    create table FIRST(
      id varchar(256),
      name varchar(256)
    );
    
    insert into FIRST values ('1', 'name1');
    insert into FIRST values ('2', 'name2');
    insert into FIRST values ('3', 'name3');
    
    CREATE TABLE second (name1 varchar(256), name2 varchar(256), name3 varchar(256)) AS
      select f1.id, f2.id, f3.id from first as f1, first as f2, first as f3
      where (f1.name = 'name1') AND (f2.name = 'name2') AND (f3.name = 'name3');